# OpenFMB sample code for NATS
Prototype of OpenFMB NATS publisher and subscriber for MeterReadingProfile in Go

## Setting up the development container when using Visual Studio Code

Follow these steps to open this sample in a local Docker development container:

1. If this is your first time using a development container, please follow the [getting started steps](https://aka.ms/vscode-remote/containers/getting-started).

2. Linux users: Update USER_UID and USER_GID in .devcontainer/Dockerfile with your user UID/GID if not 1000 to avoid creating files as root.

3. If you're not yet in a development container:
   - Clone this repository.
   - Press <kbd>F1</kbd> and select the **Remote-Containers: Open Folder in Container...** command.
   - Select the cloned copy of this folder, wait for the container to start, and try things out!

## Things to try

Once you have this sample opened in a container, you'll be able to work with it like you would locally.

> **Note:** This container runs as a non-root user with sudo access by default.
