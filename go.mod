module gitlab.com/mthollylab

require (
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/gogo/protobuf v1.3.1
	// github.com/gogo/protobuf v1.3.1
	github.com/golang/protobuf v1.4.2
	github.com/nats-io/nats-server/v2 v2.1.7 // indirect
	github.com/nats-io/nats.go v1.10.0
	gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf v1.0.0
	gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/v2 v2.0.0
	google.golang.org/protobuf v1.25.0
)

go 1.16
