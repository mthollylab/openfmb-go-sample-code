package main

import (
	"flag"
	"log"
	"math"
	"time"

	uuid "github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes/wrappers"
	nats "github.com/nats-io/nats.go"

	"github.com/gogo/protobuf/jsonpb"
	"github.com/gogo/protobuf/proto"

	commonmodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/commonmodule"
	regulatormodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/regulatormodule"
)

func getCurrentDateTime() *commonmodule.Timestamp {
	t := time.Now()
	tq := &commonmodule.TimeQuality{
		ClockFailure:         false,
		ClockNotSynchronized: false,
		LeapSecondsKnown:     true,
		TimeAccuracy:         commonmodule.TimeAccuracyKind_TimeAccuracyKind_unspecified,
	}

	timestamp := &commonmodule.Timestamp{
		Seconds:  uint64(t.Unix()),
		Fraction: uint32(float64(t.Nanosecond()) / (1000000000) * math.Pow(2, 32)),
		Tq:       tq,
	}
	return timestamp
}

func getCurrentControlDateTime() *commonmodule.ControlTimestamp {
	t := time.Now()

	timestamp := &commonmodule.ControlTimestamp{
		Seconds:  uint64(t.Unix()),
		Fraction: uint32(float64(t.Nanosecond()) / (1000000000) * math.Pow(2, 32)),
	}
	return timestamp
}

func main() {
	var urls = flag.String("s", "nats://localhost:4222", "The nats server URLs (separated by comma)")
	// var prettPrintJSON = flag.Bool("pp", false, "\"Pretty Print\" JSON data")
	// var printJSON = flag.Bool("json", false, "Print JSON data")
	var publishMRID = flag.String("mrid", "", "mRID of the recloser for publishing control messages")
	var tapPos = flag.Int("tapPos", 0, "Tap position to set in the regulator")

	log.SetFlags(0)
	flag.Parse()

	if len(*publishMRID) == 0 {
		log.Fatal("Missing mRID of recloser")
	}

	nc, err := nats.Connect(*urls)
	if err != nil {
		log.Fatalf("Can't connect: %v\n", err)
	}

	topic := "openfmb.regulatormodule.RegulatorControlProfile." + *publishMRID

	// Create the ControlMessageInfo

	miID := &commonmodule.IdentifiedObject{}
	miMRID := &wrappers.StringValue{}
	miMRID.Value = uuid.Must(uuid.NewV4()).String()
	miID.MRID = miMRID

	mi := &commonmodule.MessageInfo{}
	mi.IdentifiedObject = miID
	mi.MessageTimeStamp = getCurrentDateTime()

	cmi := &commonmodule.ControlMessageInfo{}
	cmi.MessageInfo = mi

	// Create the Recloser
	ce := &commonmodule.ConductingEquipment{}
	ce.MRID = *publishMRID

	rsys := &regulatormodule.RegulatorSystem{}
	rsys.ConductingEquipment = ce

	// Create the IED
	ied := &commonmodule.IED{}

	// Create the ControlISC to hold the TapPos
	cisc := &commonmodule.ControlISC{}
	cisc.CtlVal = int32(*tapPos)

	// Create the ATCC
	atcc := &regulatormodule.RegulatorControlATCC{}
	atcc.TapPos = cisc

	// Create the RegulatorPoint
	rp := &regulatormodule.RegulatorPoint{}
	rp.Control = atcc
	rp.StartTime = getCurrentDateTime()

	// Create the RegulatorCSG
	rpa := []*regulatormodule.RegulatorPoint{rp}

	rcsg := &regulatormodule.RegulatorCSG{}
	rcsg.CrvPts = rpa

	// Create the RegulatorControlScheduleFSCH
	fsch := &regulatormodule.RegulatorControlScheduleFSCH{}
	fsch.ValDCSG = rcsg

	// Create the RegulatorControlFSCC
	fscc := &regulatormodule.RegulatorControlFSCC{}
	fscc.RegulatorControlScheduleFSCH = fsch

	// Create the RegulatorControl
	rc := &regulatormodule.RegulatorControl{}
	rc.RegulatorControlFSCC = fscc

	// Finally create the profile
	rcp := &regulatormodule.RegulatorControlProfile{}
	rcp.ControlMessageInfo = cmi
	rcp.RegulatorSystem = rsys
	rcp.Ied = ied
	rcp.RegulatorControl = rc

	jsonMarshaler := &jsonpb.Marshaler{}
	jsonMarshaler.OrigName = true
	j, _ := jsonMarshaler.MarshalToString(rcp)
	log.Printf("JSON: " + j)

	data, err := proto.Marshal(rcp)
	if err != nil {
		log.Fatal(err)
	}
	nc.Publish(topic, data)

	nc.Flush()

	nc.Close()
}
