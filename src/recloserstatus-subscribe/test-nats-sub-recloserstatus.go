package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"log"
	"runtime"

	nats "github.com/nats-io/nats.go"

	jsonpb "github.com/gogo/protobuf/jsonpb"
	"github.com/gogo/protobuf/proto"

	// commonmodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/commonmodule"
	reclosermodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/reclosermodule"
)

func main() {
	var urls = flag.String("s", "nats://localhost:4222", "The nats server URLs (separated by comma)")
	var showTime = flag.Bool("t", false, "Display timestamps")
	var prettPrintJSON = flag.Bool("pp", false, "\"Pretty Print\" JSON data")
	var printJSON = flag.Bool("json", false, "Print JSON data")
	var subscriptionMRID = flag.String("mrid", ">", "mRID of the recloser for subscribing to messages")

	log.SetFlags(0)
	flag.Parse()

	nc, err := nats.Connect(*urls)
	if err != nil {
		log.Fatalf("Can't connect: %v\n", err)
	}

	topic := "openfmb.reclosermodule.RecloserStatusProfile." + *subscriptionMRID

	jsonMarshaler := &jsonpb.Marshaler{}
	jsonMarshaler.OrigName = true

	rsp := &reclosermodule.RecloserStatusProfile{}

	var firstMRID = ""
	var currentMRID = ""

	nc.Subscribe(topic, func(msg *nats.Msg) {
		log.Printf("Subject: %s", msg.Subject)
		log.Printf("Payload size: %d", len(msg.Data))
		err := proto.Unmarshal(msg.Data, rsp)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("Message mRID: " + rsp.StatusMessageInfo.MessageInfo.IdentifiedObject.MRID.Value)
		// log.Printf("Meter mRID:   " + mrp.Meter.ConductingEquipment.MRID)
		ts := rsp.StatusMessageInfo.MessageInfo.MessageTimeStamp
		log.Printf("Message Timestamp: %d.%d", ts.Seconds, ts.Fraction)

		j, _ := jsonMarshaler.MarshalToString(rsp)

		if firstMRID == "" {
			firstMRID = rsp.StatusMessageInfo.MessageInfo.IdentifiedObject.MRID.Value
		} else {
			currentMRID = rsp.StatusMessageInfo.MessageInfo.IdentifiedObject.MRID.Value
			if currentMRID == firstMRID {
				runtime.Goexit()
			}
		}

		if *printJSON || *prettPrintJSON {
			if *prettPrintJSON {
				var jPretty bytes.Buffer
				json.Indent(&jPretty, []byte(j), "", "  ")
				log.Printf("JSON: " + string(jPretty.Bytes()))
			} else {
				log.Printf("JSON: " + j)
			}
		}

		rs := rsp.RecloserStatus
		if rs != nil {
			xcbr := rs.StatusAndEventXCBR
			if xcbr != nil {
				pos := xcbr.Pos
				if pos != nil {
					log.Printf("  Pos: %s", pos.StVal.String())
				}
			}
		} else {
			log.Print("RecloserStatus missing...")
		}

		log.Printf("\n")
	})
	nc.Flush()

	if err := nc.LastError(); err != nil {
		log.Fatal(err)
	}

	log.Printf("Listening on [%s]\n", topic)
	if *showTime {
		log.SetFlags(log.LstdFlags)
	}

	runtime.Goexit()
}
