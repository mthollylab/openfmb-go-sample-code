package main

import (
	"flag"
	"log"
	"math"
	"time"

	uuid "github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes/wrappers"
	nats "github.com/nats-io/nats.go"

	"github.com/gogo/protobuf/jsonpb"
	"github.com/gogo/protobuf/proto"

	commonmodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/commonmodule"
	switchmodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/switchmodule"
)

func getCurrentDateTime() *commonmodule.Timestamp {
	t := time.Now()
	tq := &commonmodule.TimeQuality{
		ClockFailure:         false,
		ClockNotSynchronized: false,
		LeapSecondsKnown:     true,
		TimeAccuracy:         commonmodule.TimeAccuracyKind_TimeAccuracyKind_unspecified,
	}

	timestamp := &commonmodule.Timestamp{
		Seconds:  uint64(t.Unix()),
		Fraction: uint32(float64(t.Nanosecond()) / (1000000000) * math.Pow(2, 32)),
		Tq:       tq,
	}
	return timestamp
}

func main() {
	var urls = flag.String("s", "nats://localhost:4222", "The nats server URLs (separated by comma)")
	// var prettPrintJSON = flag.Bool("pp", false, "\"Pretty Print\" JSON data")
	// var printJSON = flag.Bool("json", false, "Print JSON data")
	var publishMRID = flag.String("mrid", "", "mRID of the switch for publishing control messages")
	var isClose = flag.Bool("c", false, "Trip/Close - Close = true")

	log.SetFlags(0)
	flag.Parse()

	if len(*publishMRID) == 0 {
		log.Fatal("Missing mRID of switch")
	}

	nc, err := nats.Connect(*urls)
	if err != nil {
		log.Fatalf("Can't connect: %v\n", err)
	}

	topic := "openfmb.switchmodule.SwitchControlProfile." + *publishMRID

	// Create the ControlMessageInfo

	miID := &commonmodule.IdentifiedObject{}
	miID.MRID = &wrappers.StringValue{
		Value: uuid.Must(uuid.NewV4()).String(),
	}

	mi := &commonmodule.MessageInfo{}
	mi.IdentifiedObject = miID
	mi.MessageTimeStamp = getCurrentDateTime()

	cmi := &commonmodule.ControlMessageInfo{}
	cmi.MessageInfo = mi

	// Create the ProtectedSwitch
	ce := &commonmodule.ConductingEquipment{}
	ce.MRID = *publishMRID

	ps := &switchmodule.ProtectedSwitch{}
	ps.ConductingEquipment = ce

	// Create the IED
	ied := &commonmodule.IED{}

	// Create the SwitchPoint
	cdpc := &commonmodule.ControlDPC{}
	cdpc.CtlVal = *isClose

	sp := &commonmodule.SwitchPoint{}
	sp.Pos = cdpc

	// Create the SwitchSSG
	spa := []*commonmodule.SwitchPoint{sp}

	scsg := &commonmodule.SwitchCSG{}
	scsg.CrvPts = spa

	// Create the SwitchControlScheduleFSCH
	fsch := &commonmodule.SwitchControlScheduleFSCH{}
	fsch.ValDCSG = scsg

	// Create the SwitchControlFSCC
	fscc := &switchmodule.SwitchControlFSCC{}
	fscc.SwitchControlScheduleFSCH = fsch

	// Create the SwitchControl
	sc := &switchmodule.SwitchControl{}
	sc.SwitchControlFSCC = fscc

	// Finally create the profile
	rcp := &switchmodule.SwitchControlProfile{}
	rcp.ControlMessageInfo = cmi
	rcp.ProtectedSwitch = ps
	rcp.Ied = ied
	rcp.SwitchControl = sc

	jsonMarshaler := &jsonpb.Marshaler{}
	jsonMarshaler.OrigName = true
	j, _ := jsonMarshaler.MarshalToString(rcp)
	log.Printf("JSON: " + j)

	data, err := proto.Marshal(rcp)
	if err != nil {
		log.Fatal(err)
	}
	nc.Publish(topic, data)

	nc.Flush()

	nc.Close()
}
