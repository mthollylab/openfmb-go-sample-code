package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"log"
	"math"
	"time"

	uuid "github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes/wrappers"
	nats "github.com/nats-io/nats.go"

	"github.com/gogo/protobuf/jsonpb"
	"github.com/gogo/protobuf/proto"

	commonmodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/commonmodule"
	essmodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/essmodule"
)

func getCurrentDateTime() *commonmodule.Timestamp {
	t := time.Now()

	tq := &commonmodule.TimeQuality{
		ClockFailure:         false,
		ClockNotSynchronized: false,
		LeapSecondsKnown:     true,
		TimeAccuracy:         commonmodule.TimeAccuracyKind_TimeAccuracyKind_unspecified,
	}

	timestamp := &commonmodule.Timestamp{
		Seconds:  uint64(t.Unix()),
		Fraction: uint32(float64(t.Nanosecond()) / (1000000000) * math.Pow(2, 32)),
		Tq:       tq,
	}

	return timestamp
}

func getCurrentControlDateTime() *commonmodule.ControlTimestamp {
	t := time.Now()

	timestamp := &commonmodule.ControlTimestamp{
		Seconds:  uint64(t.Unix()),
		Fraction: uint32(float64(t.Nanosecond()) / (1000000000) * math.Pow(2, 32)),
	}
	return timestamp
}

func main() {
	var urls = flag.String("s", "nats://localhost:4222", "The nats server URLs (separated by comma)")
	var prettPrintJSON = flag.Bool("pp", false, "\"Pretty Print\" JSON data")
	var printJSON = flag.Bool("json", true, "Print JSON data")
	var publishMRID = flag.String("mrid", "", "mRID of the ESS for publishing control messages")
	var pref = flag.Float64("p", 0.0, "P reference value")
	var qref = flag.Float64("q", 0.0, "Q reference value")
	var test = flag.Bool("test", false, "If true, don't actually publish the control message")

	log.SetFlags(0)
	flag.Parse()

	if len(*publishMRID) == 0 {
		log.Fatal("Missing mRID of recloser")
	}

	nc, err := nats.Connect(*urls)
	if err != nil {
		log.Fatalf("Can't connect: %v\n", err)
	}

	topic := "openfmb.essmodule.ESSControlProfile." + *publishMRID
	log.Printf("Topic: %s", topic)

	// Create the ControlMessageInfo

	miID := &commonmodule.IdentifiedObject{}
	miMRID := &wrappers.StringValue{}
	miMRID.Value = uuid.Must(uuid.NewV4()).String()
	miID.MRID = miMRID

	mi := &commonmodule.MessageInfo{}
	mi.IdentifiedObject = miID
	mi.MessageTimeStamp = getCurrentDateTime()

	cmi := &commonmodule.ControlMessageInfo{}
	cmi.MessageInfo = mi

	// Create the ESS
	ce := &commonmodule.ConductingEquipment{}
	ce.MRID = *publishMRID

	ess := &commonmodule.ESS{}
	ess.ConductingEquipment = ce

	// Create the IED
	ied := &commonmodule.IED{}

	// Create the SchedulePoint value for both P and Q
	pScheduleParameter := &commonmodule.ENG_ScheduleParameter{}
	pScheduleParameter.ScheduleParameterType = commonmodule.ScheduleParameterKind_ScheduleParameterKind_W_net_mag
	pScheduleParameter.Value = float32(*pref)

	qScheduleParameter := &commonmodule.ENG_ScheduleParameter{}
	qScheduleParameter.ScheduleParameterType = commonmodule.ScheduleParameterKind_ScheduleParameterKind_VAr_net_mag
	qScheduleParameter.Value = float32(*qref)

	sparms := []*commonmodule.ENG_ScheduleParameter{pScheduleParameter, qScheduleParameter}

	schedPt := &commonmodule.SchedulePoint{}
	schedPt.ScheduleParameter = sparms
	schedPt.StartTime = getCurrentControlDateTime()

	spa := []*commonmodule.SchedulePoint{schedPt}

	scsg := &commonmodule.ScheduleCSG{}
	scsg.SchPts = spa

	// Create the ControlScheduleFSCH
	fsch := &commonmodule.ControlScheduleFSCH{}
	fsch.ValACSG = scsg

	// Create the ControlFSCC
	fscc := &commonmodule.ControlFSCC{}
	fscc.ControlScheduleFSCH = fsch

	// Create teh ESSControlFSCC
	efscc := &essmodule.EssControlFSCC{}
	efscc.ControlFSCC = fscc

	// Create the ESSControl
	ec := &essmodule.ESSControl{}
	ec.EssControlFSCC = efscc

	// Finally create the profile
	ecp := &essmodule.ESSControlProfile{}
	ecp.Ess = ess
	ecp.ControlMessageInfo = cmi
	ecp.EssControl = ec
	ecp.Ied = ied

	if *printJSON || *prettPrintJSON {
		jsonMarshaler := &jsonpb.Marshaler{}
		jsonMarshaler.OrigName = true
		j, _ := jsonMarshaler.MarshalToString(ecp)

		if *prettPrintJSON {
			var jPretty bytes.Buffer
			json.Indent(&jPretty, []byte(j), "", "  ")
			log.Printf("JSON: " + string(jPretty.Bytes()))
		} else {
			log.Printf("JSON: " + j)
		}
	}

	if !*test {
		data, err := proto.Marshal(ecp)
		if err != nil {
			log.Fatal(err)
		}
		nc.Publish(topic, data)

		nc.Flush()
	}

	nc.Close()
}
