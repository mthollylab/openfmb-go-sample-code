package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"log"
	"os"
	"runtime"

	nats "github.com/nats-io/nats.go"

	// jsonpb "github.com/golang/protobuf/jsonpb"
	// "github.com/golang/protobuf/proto"

	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"

	// metermodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/metermodule"
	metermodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/v2/openfmb/metermodule"
)

func main() {
	var urls = flag.String("s", "nats://localhost:4222", "The nats server URLs (separated by comma)")
	var showTime = flag.Bool("t", false, "Display timestamps")
	var prettPrintJSON = flag.Bool("pp", false, "\"Pretty Print\" JSON data")
	var printJSON = flag.Bool("json", false, "Print JSON data")
	var subscriptionMRID = flag.String("mrid", ">", "mRID of the meter for subscribing to messages")

	log.SetFlags(0)
	flag.Parse()

	var outputFile, err = os.Create("go-json.txt")
	if err != nil {
		log.Fatalf("Can't open output file: %v\n", err)
	}
	// defer outputFile.Close()

	nc, err := nats.Connect(*urls)
	if err != nil {
		log.Fatalf("Can't connect: %v\n", err)
	}

	topic := "openfmb.metermodule.MeterReadingProfile." + *subscriptionMRID

	// jsonMarshaler := &jsonpb.Marshaler{}
	// jsonMarshaler.OrigName = true

	mrp := &metermodule.MeterReadingProfile{}

	nc.Subscribe(topic, func(msg *nats.Msg) {
		log.Printf("Subject: %s", msg.Subject)
		log.Printf("Payload size: %d", len(msg.Data))
		err := proto.Unmarshal(msg.Data, mrp)
		if err != nil {
			log.Printf("Error parsing protobuf: %v", err)
		} else {
			if mrp.ReadingMessageInfo != nil {
				rmi := mrp.ReadingMessageInfo
				if rmi.MessageInfo != nil {
					mi := rmi.MessageInfo

					if mi.IdentifiedObject != nil {
						if mi.IdentifiedObject.MRID != nil {
							if len(mi.IdentifiedObject.MRID.Value) != 0 {
								log.Printf("Message mRID: " + mrp.ReadingMessageInfo.MessageInfo.IdentifiedObject.MRID.Value)
							}
						}
					}

					if mi.MessageTimeStamp != nil {
						// log.Printf("Meter mRID:   " + mrp.Meter.ConductingEquipment.MRID)
						ts := mrp.ReadingMessageInfo.MessageInfo.MessageTimeStamp

						// log.Printf("Message Timestamp: %d.%d", ts.Seconds, ts.Fraction)
						log.Printf("Message Timestamp: %d.%d", ts.Seconds, ts.Nanoseconds)
					}
				}
			}

			// j, _ := jsonMarshaler.MarshalToString(mrp)
			j := protojson.Format(mrp)

			_, err = outputFile.WriteString(j + "\n")
			if err != nil {
				log.Fatal(err)
			} else {
				// log.Printf("wrote %d bytes\n", n3)
			}
			outputFile.Sync()

			if *printJSON || *prettPrintJSON {
				if *prettPrintJSON {
					var jPretty bytes.Buffer
					json.Indent(&jPretty, []byte(j), "", "  ")
					log.Printf("JSON: " + string(jPretty.Bytes()))
				} else {
					log.Printf("JSON: " + j)
				}
			}

			mmxu := mrp.MeterReading.ReadingMMXU
			if mmxu != nil {
				phv := mmxu.PhV
				if phv != nil {
					net := phv.Net
					if net != nil {
						log.Printf("  ReadingMMXU.PhV.Net.cVal.mag: %.1f", net.CVal.Mag)
					}
				}
			}

			log.Printf("\n")
		}
	})
	nc.Flush()

	if err := nc.LastError(); err != nil {
		log.Fatal(err)
	}

	log.Printf("Listening on [%s]\n", topic)
	if *showTime {
		log.SetFlags(log.LstdFlags)
	}

	runtime.Goexit()
}
