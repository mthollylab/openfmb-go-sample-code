package main

import (
	"bufio"
	"flag"
	"log"
	"math"
	"os"
	"reflect"
	"strings"
	"time"

	"github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes/wrappers"
	nats "github.com/nats-io/nats.go"

	jsonpb "github.com/gogo/protobuf/jsonpb"
	"github.com/gogo/protobuf/proto"

	commonmodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/commonmodule"
	metermodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/metermodule"
)

func getCurrentDateTime() *commonmodule.Timestamp {
	t := time.Now()
	tq := &commonmodule.TimeQuality{
		ClockFailure:         false,
		ClockNotSynchronized: false,
		LeapSecondsKnown:     true,
		TimeAccuracy:         commonmodule.TimeAccuracyKind_TimeAccuracyKind_unspecified,
	}

	timestamp := &commonmodule.Timestamp{
		Seconds:  uint64(t.Unix()),
		Fraction: uint32(float64(t.Nanosecond()) / (1000000000) * math.Pow(2, 32)),
		Tq:       tq,
	}
	return timestamp
}

func getOpenFMBTopicName(t reflect.Type) string {
	pkgPath := t.PkgPath()
	openfmbTopic := pkgPath[strings.LastIndex(pkgPath, "openfmb/"):]
	openfmbTopic = openfmbTopic + "/" + t.Name()
	return strings.Replace(openfmbTopic, "/", ".", -1)
}

func main() {
	var urls = flag.String("s", "nats://localhost:4222", "The nats server URLs (separated by comma)")
	var inputFileName = flag.String("i", "replay-capture.txt", "JSON file of MeterReadingProfile messages to publish")

	log.SetFlags(0)
	flag.Parse()

	nc, err := nats.Connect(*urls)
	if err != nil {
		log.Fatalf("Can't connect: %v\n", err)
	}

	topic := "openfmb.metermodule.MeterReadingProfile."

	// messageMrid := uuid.NewV4()
	// meterMrid := uuid.NewV4()

	file, err := os.Open(*inputFileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		json := scanner.Text()
		log.Println("JSON: " + json)

		mrp := &metermodule.MeterReadingProfile{}

		miID := &commonmodule.IdentifiedObject{}
		miID.MRID = &wrappers.StringValue{
			Value: uuid.Must(uuid.NewV4()).String(),
		}

		mi := &commonmodule.MessageInfo{}
		mi.IdentifiedObject = miID
		mi.MessageTimeStamp = getCurrentDateTime()

		rmi := &commonmodule.ReadingMessageInfo{}
		rmi.MessageInfo = mi

		mrp.ReadingMessageInfo = rmi

		jsonpb.UnmarshalString(json, mrp)

		data, _ := proto.Marshal(mrp)

		log.Println("Message mRID: " + mrp.ReadingMessageInfo.MessageInfo.IdentifiedObject.MRID.Value)

		subTopic := mrp.Meter.ConductingEquipment.MRID

		nc.Publish(topic+subTopic, data)

		time.Sleep(time.Second * 1)
	}
}
