package main

import (
	"log"
	"math"
	"time"

	commonmodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/commonmodule"
)

func convertToOpenFMBTimestamp(t time.Time) *commonmodule.Timestamp {
	tq := &commonmodule.TimeQuality{
		ClockFailure:         false,
		ClockNotSynchronized: false,
		LeapSecondsKnown:     true,
		TimeAccuracy:         commonmodule.TimeAccuracyKind_TimeAccuracyKind_unspecified,
	}

	timestamp := &commonmodule.Timestamp{
		Seconds:  uint64(t.Unix()),
		Fraction: uint32(float64(t.Nanosecond()) / (1000000000) * math.Pow(2, 32)),
		Tq:       tq,
	}
	return timestamp
}

func convertToTime(t commonmodule.Timestamp) time.Time {
	nanos := uint32(float64(int64(t.Fraction)*int64(1000000000)) / (math.Pow(2, 32)))

	mytime := time.Unix(int64(t.Seconds), int64(nanos))

	return mytime
}

func main() {
	currentTime := time.Now()

	log.Println("Current time:")
	log.Printf("  Seconds: %d\n", currentTime.Unix())
	log.Printf("  Nanos: %d\n", currentTime.Nanosecond())

	openfmbTimestamp := convertToOpenFMBTimestamp(currentTime)

	log.Println("OpenFMB Timestamp:")
	log.Printf("  Seconds: %d\n", openfmbTimestamp.Seconds)
	log.Printf("  Fraction: %d\n", openfmbTimestamp.Fraction)

	newTime := convertToTime(*openfmbTimestamp)

	log.Println("New time:")
	log.Printf("  Seconds: %d\n", newTime.Unix())
	log.Printf("  Nanos: %d\n", newTime.Nanosecond())
}
