package main

import (
	"flag"
	"log"
	"math"
	"time"

	uuid "github.com/gofrs/uuid"
	"github.com/golang/protobuf/ptypes/wrappers"
	nats "github.com/nats-io/nats.go"

	"github.com/gogo/protobuf/jsonpb"
	"github.com/gogo/protobuf/proto"

	breakermodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/breakermodule"
	commonmodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/commonmodule"
)

func getCurrentDateTime() *commonmodule.Timestamp {
	t := time.Now()
	tq := &commonmodule.TimeQuality{
		ClockFailure:         false,
		ClockNotSynchronized: false,
		LeapSecondsKnown:     true,
		TimeAccuracy:         commonmodule.TimeAccuracyKind_TimeAccuracyKind_unspecified,
	}

	timestamp := &commonmodule.Timestamp{
		Seconds:  uint64(t.Unix()),
		Fraction: uint32(float64(t.Nanosecond()) / (1000000000) * math.Pow(2, 32)),
		Tq:       tq,
	}
	return timestamp
}

func main() {
	var urls = flag.String("s", "nats://localhost:4222", "The nats server URLs (separated by comma)")
	// var prettPrintJSON = flag.Bool("pp", false, "\"Pretty Print\" JSON data")
	// var printJSON = flag.Bool("json", false, "Print JSON data")
	var publishMRID = flag.String("mrid", "", "mRID of the breaker for publishing control messages")
	var isClose = flag.Bool("c", false, "Trip/Close - Close = true")

	log.SetFlags(0)
	flag.Parse()

	if len(*publishMRID) == 0 {
		log.Fatal("Missing mRID of breaker")
	}

	nc, err := nats.Connect(*urls)
	if err != nil {
		log.Fatalf("Can't connect: %v\n", err)
	}

	topic := "openfmb.breakermodule.BreakerDiscreteControlProfile." + *publishMRID

	// Create the ControlMessageInfo

	miID := &commonmodule.IdentifiedObject{}
	miMRID := &wrappers.StringValue{}
	miMRID.Value = uuid.Must(uuid.NewV4()).String()
	miID.MRID = miMRID

	mi := &commonmodule.MessageInfo{}
	mi.IdentifiedObject = miID
	mi.MessageTimeStamp = getCurrentDateTime()

	cmi := &commonmodule.ControlMessageInfo{}
	cmi.MessageInfo = mi

	// Create the Breaker
	ce := &commonmodule.ConductingEquipment{}
	ce.MRID = *publishMRID

	bkr := &breakermodule.Breaker{}
	bkr.ConductingEquipment = ce

	// Create the IED
	ied := &commonmodule.IED{}

	// Create the BreakerDiscreteControl
	cdpc := &commonmodule.ControlDPC{}
	cdpc.CtlVal = *isClose

	xcbr := &breakermodule.BreakerDiscreteControlXCBR{}
	xcbr.Pos = cdpc

	bdc := &breakermodule.BreakerDiscreteControl{}
	bdc.BreakerDiscreteControlXCBR = xcbr

	// Finally create the profile
	bdcp := &breakermodule.BreakerDiscreteControlProfile{}
	bdcp.ControlMessageInfo = cmi
	bdcp.Breaker = bkr
	bdcp.Ied = ied
	bdcp.BreakerDiscreteControl = bdc

	jsonMarshaler := &jsonpb.Marshaler{}
	jsonMarshaler.OrigName = true
	j, _ := jsonMarshaler.MarshalToString(bdcp)
	log.Printf("JSON: " + j)

	data, err := proto.Marshal(bdcp)
	if err != nil {
		log.Fatal(err)
	}
	nc.Publish(topic, data)

	nc.Flush()

	nc.Close()
}
