package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"log"
	"runtime"

	nats "github.com/nats-io/nats.go"

	jsonpb "github.com/gogo/protobuf/jsonpb"
	"github.com/gogo/protobuf/proto"

	// commonmodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/commonmodule"
	breakermodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/breakermodule"
)

func main() {
	var urls = flag.String("s", "nats://localhost:4222", "The nats server URLs (separated by comma)")
	var showTime = flag.Bool("t", false, "Display timestamps")
	var prettPrintJSON = flag.Bool("pp", false, "\"Pretty Print\" JSON data")
	var printJSON = flag.Bool("json", false, "Print JSON data")
	var subscriptionMRID = flag.String("mrid", ">", "mRID of the breaker for subscribing to messages")

	log.SetFlags(0)
	flag.Parse()

	nc, err := nats.Connect(*urls)
	if err != nil {
		log.Fatalf("Can't connect: %v\n", err)
	}

	topic := "openfmb.breakermodule.BreakerReadingProfile." + *subscriptionMRID

	jsonMarshaler := &jsonpb.Marshaler{}
	jsonMarshaler.OrigName = true

	brp := &breakermodule.BreakerReadingProfile{}

	nc.Subscribe(topic, func(msg *nats.Msg) {
		log.Printf("Subject: %s", msg.Subject)
		log.Printf("Payload size: %d", len(msg.Data))
		err := proto.Unmarshal(msg.Data, brp)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("Message mRID: " + brp.ReadingMessageInfo.MessageInfo.IdentifiedObject.MRID.Value)
		ts := brp.ReadingMessageInfo.MessageInfo.MessageTimeStamp
		log.Printf("Message Timestamp: %d.%d", ts.Seconds, ts.Fraction)

		j, _ := jsonMarshaler.MarshalToString(brp)

		if *printJSON || *prettPrintJSON {
			if *prettPrintJSON {
				var jPretty bytes.Buffer
				json.Indent(&jPretty, []byte(j), "", "  ")
				log.Printf("JSON: " + string(jPretty.Bytes()))
			} else {
				log.Printf("JSON: " + j)
			}
		}

		mmxu := brp.BreakerReading[0].ReadingMMXU
		if mmxu != nil {
			hz := mmxu.Hz
			if hz != nil {
				log.Printf("  Hz: %.1f", hz.Mag.F.Value)
			}
		}

		log.Printf("\n")
	})
	nc.Flush()

	if err := nc.LastError(); err != nil {
		log.Fatal(err)
	}

	log.Printf("Listening on [%s]\n", topic)
	if *showTime {
		log.SetFlags(log.LstdFlags)
	}

	runtime.Goexit()
}
